﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Controls;
using PrestaClientViaAvalonia.Models;
using PrestaClientViaAvalonia.Services;

namespace PrestaClientViaAvalonia.ViewModels
{
    public class StatsPageViewModel : BaseViewModel
    {
        private bool _canExecute;
        private string _buttonName;
        private string _info;
        private YandexCounter _selectedSite;
        private AnalyticsService _analyticsService;
        private string _selectedDate;
        public ObservableCollection<string> Dates { get; set; }
        private ICommand _statsCommand;
        private ICommand _downloadCommand;
        public ObservableCollection<YandexCounter> Stats { get; set; }
        public StatsPageViewModel()
        {
            Dates = new ObservableCollection<string>
            {
                "Неделя",
                "Месяц",
                "Квартал"
            };
            SelectedDate = Dates[1];
            ButtonName = "Загрузить";
            _analyticsService = new AnalyticsService();
            _canExecute = true;
            Stats = new ObservableCollection<YandexCounter>();
        }

        public YandexCounter SelectedSite
        {
            get { return _selectedSite; }
            set
            {
                _selectedSite = value;
                OnPropertyChanged("SelectedSite");
            }
        }

        public string SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                _selectedDate = value;
                OnPropertyChanged("SelectedDate");
            }
        }

        public string Info
        {
            get => _info;
            set
            {
                _info = value;
                OnPropertyChanged("Info");
            }
        }

        public string ButtonName
        {
            get { return _buttonName; }
            set
            {
                _buttonName = value;
                OnPropertyChanged("ButtonName");
            }
        }

        public ICommand DownloadCommand
        {
            get
            {
                return _downloadCommand ?? (_downloadCommand
                           = new CommandHandler(() => OnDownload(), _canExecute));
            }
        }

        public ICommand StatsCommand
        {
            get
            {
                return _statsCommand ?? (_statsCommand
                           = new CommandHandler(() => GetStats(), _canExecute));
            }
        }

        public async void GetStats()
        {
            Info = "Загрузка...";
            var stats = await _analyticsService.GetStats();
            Stats.Clear();
            foreach (var stat in stats)
            {
                Stats.Add(stat);
            }
            ButtonName = "Обновить";
            Info = $"Данные обновлены {DateTime.Now}";
        }

        public async void OnDownload()
        {
            var path = await OpenFileDialog();
            if(path != null)
            await _analyticsService.DownloadStats(path, SelectedSite.Site,SelectedDate);
            Info = $"Статистика запросов для {SelectedSite.Site} за период {SelectedDate} загружена";
        }

        private async Task<string>  OpenFileDialog()
        {
            var saveDialog = new SaveFileDialog();
            saveDialog.Title = "Выберите место";
            saveDialog.InitialFileName = $"{SelectedSite.Site}_Info.csv";
            saveDialog.DefaultExtension = "csv";
            var path = await saveDialog.ShowAsync(new Window());
            return path;
        }
    }
}