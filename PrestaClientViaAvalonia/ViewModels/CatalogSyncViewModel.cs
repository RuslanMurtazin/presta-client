﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Controls;
using PrestaClientViaAvalonia.Models;
using PrestaClientViaAvalonia.Services;

namespace PrestaClientViaAvalonia.ViewModels
{
    public class CatalogSyncViewModel : BaseViewModel
    {
        private bool _canExecute;
        private string _info;
        private bool _booleanFlag;
        private PrestaService _prestaService;
        private ICommand _checkCommand;
        private Site _selectedSite;
        public ObservableCollection<Site> Sites { get; set; }

        public CatalogSyncViewModel()
        {
            Sites = new ObservableCollection<Site>(new List<Site>
            {
                new Site{Url = "Neotren"},
                new Site{Url = "Weekend-billiard"}
            });
            SelectedSite = Sites[0];
            BooleanFlag = false;
            _canExecute = true;
        }

        public Site SelectedSite
        {
            get { return _selectedSite; }
            set
            {
                _selectedSite = value;
                OnPropertyChanged("SelectedSite");
            }
        }

        public string Info
        {
            get { return _info; }
            set
            {
                _info = value;
                OnPropertyChanged("Info");
            }
        }
        public bool BooleanFlag
        {
            get { return _booleanFlag; }
            set
            {
                _booleanFlag = value;
                OnPropertyChanged("BooleanFlag");
            }
        }

        public ICommand CheckCommand
        {
            get { return _checkCommand ?? (_checkCommand = new CommandHandler(() => CheckUpdates(), _canExecute)); }
        }

        private async void CheckUpdates()
        {
            _prestaService = new PrestaService(ConfigurationManager.AppSettings["currentSite"]);
            var count = 0;
            var path = "";
            if(BooleanFlag)
            path = await OpenDialog();
            Info = "Выполняется анализ цен...";
            await Task.Run(() =>
            {
                if(SelectedSite.Url == "Neotren")
                    count = _prestaService.SyncCatalogsNeotren(BooleanFlag,path);
                if (SelectedSite.Url == "Weekend-billiard")
                    count = _prestaService.SyncCatalogsWeekBld(BooleanFlag, path);
            });

            Info = count > 0
                ? $"Анализ завершен. Цены исправлены. Общее количество: {count}"
                : "Анализ завершен. Все цены актуальны. Исправления не требуются.";
        }

        private async Task<string> OpenDialog()
        {
            var path = "";
            var saveDialog = new SaveFileDialog();
            saveDialog.Title = "Выберите место";
            saveDialog.InitialFileName = $"Отчет об изменении цен.csv";
            saveDialog.DefaultExtension = "csv";
            path = await saveDialog.ShowAsync(new Window());
            return path;
        }
    }
}
