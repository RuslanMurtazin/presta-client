﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrestaClientViaAvalonia.Services;
using ReactiveUI;
using System.Reactive.Linq;
using System.Windows.Input;
using Avalonia.Reactive;
using OmniXaml.ObjectAssembler;


namespace PrestaClientViaAvalonia.ViewModels
{
    public class AvaloniaViewModel : ReactiveObject
    {
    
        private string _text;

        public AvaloniaViewModel()
        {
            //Sum = new Command(OnSum);            
        }

        public string Text
        {
            get { return _text; }
            set { this.RaiseAndSetIfChanged(ref _text, value); }
        }
        
        //public ICommand ViewText { get; }

        //private void OnShow()
        //{
        //    ViewText.
        //}

        public ICommand ViewText { get; }

        public void ShowText()
        {
            Text = "KEK";
            //ViewText
        }
        public IObservable<string> Now { get; } =
            Observable.Timer(DateTimeOffset.Now, new TimeSpan(0, 0, 0, 1))
                .Select(_ => DateTime.Now.ToString());

    }
}
