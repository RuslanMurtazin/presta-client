﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reactive.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Controls;
using Bukimedia.PrestaSharp.Entities;
using PrestaClientViaAvalonia.Models;
using PrestaClientViaAvalonia.Services;

namespace PrestaClientViaAvalonia.ViewModels
{
    public class CustomViewModel : BaseViewModel
    {
        public ObservableCollection<product> Products { get; set; }
        public ObservableCollection<TemporaryCategory> Categories { get; set; }
        public ObservableCollection<TemporaryCategory> SelectedCategories { get; set; }
        private PrestaService _prestaService;
        private string filepath;
        private string text;
        private string _site;
        private bool _booleanFlag = false;

        public CustomViewModel()
        {
            SelectedCategories = new ObservableCollection<TemporaryCategory>();
            Categories = new ObservableCollection<TemporaryCategory>();
            Categories.Add(new TemporaryCategory{Id = 1, Name = "Здесь будут отображаться категории магазина"});
            _canExecute = true;
        }
        private ICommand _downloadCommand;
        private ICommand _textCommand;

        public ICommand TextCommand
        {
            get { return _textCommand ?? (_textCommand
                    = new CommandHandler(() => TextAction(), _canExecute)); }
        }
        private ICommand _updateCommand;

        public ICommand UpdateCommand
        {
            get { return _updateCommand ?? (_updateCommand = new CommandHandler(() => UpdateAction(), _canExecute)); }
        }
        public ICommand DownloadCommand
        {
            get
            {
                return _downloadCommand ?? (_downloadCommand = new CommandHandler(() => DownloadAction(), _canExecute));
            }
        }

        public bool BooleanFlag
        {
            get { return _booleanFlag; }
            set
            {
                _booleanFlag = value;
                OnPropertyChanged("BooleanFlag");
            }
        }

        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }

        private bool _canExecute;

        public async void UpdateAction()
        {
            var path = await OpenDialog(false);
            _prestaService = new PrestaService(ConfigurationManager.AppSettings["currentSite"]);
            Text = "Обновляем товары...";
            await Task.Run(() =>
            {
                if(path != null)
                _prestaService.UpdateProducts(path);
            });
            Text = "Товары успешно обновлены!";
        }
        public async void DownloadAction()
        {
            _prestaService = new PrestaService(ConfigurationManager.AppSettings["currentSite"]);
            var tmpProducts = new List<product>();
            var path = await OpenDialog(true);
            Text = "Загружаем товары...";
            if (path != null)
                await Task.Run(() =>
                {
                    if (BooleanFlag)
                        _prestaService.GetProductsByFilter(SelectedCategories.ToList(),path);
                    else
                    {
                        tmpProducts = _prestaService.GetProducts(path);
                    }
                });
            Text = "Загрузка успешно завершена";
            //foreach (var product in tmpProducts)
            //{
            //    Products.Add(product);
            //}
        }

        public async void TextAction()
        {
            _prestaService = new PrestaService(ConfigurationManager.AppSettings["currentSite"]);
            var tmpVar = Categories.Count;
            var info = 0;
            var cats = new List<TemporaryCategory>();
            await Task.Run(() =>
            {
                info = _prestaService.GetProductsCount();
                cats = _prestaService.GetCategories();
            });
            Text = $"В данный момент в каталоге {info} товаров, загружено {cats.Count} категорий";
            Categories.Clear();
            foreach (var cat in cats)
            {
                Categories.Add(cat);
            }
        }

        private async Task<string> OpenDialog(bool isDownload)
        {
            var path = "";
            if (isDownload)
            {
                var saveDialog = new SaveFileDialog();
                saveDialog.Title = "Выберите место";
                saveDialog.InitialFileName = $"{ConfigurationManager.AppSettings["currentSite"]}_каталог.csv";
                saveDialog.DefaultExtension = "csv";
                path = await saveDialog.ShowAsync(new Window());
            }
            else
            {
                var openDialog = new OpenFileDialog();
                var file = await openDialog.ShowAsync(new Window());
                path = file[0];
            }
            return path;
        }

    }
}
