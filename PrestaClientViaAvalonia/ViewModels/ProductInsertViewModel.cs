﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Controls;
using Bukimedia.PrestaSharp.Entities.AuxEntities;
using PrestaClientViaAvalonia.Models;
using PrestaClientViaAvalonia.Services;
using ReactiveUI;
using SharpDX.Direct3D11;
using product = Bukimedia.PrestaSharp.Entities.product;

namespace PrestaClientViaAvalonia.ViewModels
{
    public class ProductInsertViewModel : BaseViewModel
    {
        private PrestaService _prestaService;
        public ObservableCollection<TemporaryCategory> Categories { get; set; }
        public ObservableCollection<TemporaryCategory> SelectedCategories { get; set; }
        public List<string> Images = new List<string>();
        private string name;
        private string description;
        private string price;
        private string status;
        private string _warning;
        private string _filter;
        private string _imagesCount;
        private bool _canExecute;
        private bool _booleanFlag;
        private ICommand _addCommand;
        private ICommand _openDialogCommand;
        private ICommand _updateCategories;
        public bool BooleanFlag { get => _booleanFlag; set { _booleanFlag = value; OnPropertyChanged("BooleanFlag"); } }
        public string ImagesCount { get => _imagesCount; set { _imagesCount = value; OnPropertyChanged("ImagesCount"); } }
        public string Name { get => name; set { name = value; OnPropertyChanged("Name"); } }
        public string Filter { get => _filter; set { _filter = value; OnPropertyChanged("Filter"); } }
        public string Description
        {
            get => description;
            set { description = value; OnPropertyChanged("Description"); }
        }
        public string Price { get => price; set { price = value; OnPropertyChanged("Price"); } }
        public string Status { get => status; set { status = value; OnPropertyChanged("Status"); } }
        public string Warning { get => _warning; set { _warning = value; OnPropertyChanged("Warning"); } }
        public ProductInsertViewModel()
        {
            BooleanFlag = true;
            SelectedCategories = new ObservableCollection<TemporaryCategory>();
            Categories = new ObservableCollection<TemporaryCategory>();
            _canExecute = true;
        }
        public ICommand AddCommand
        {
            get { return _addCommand ?? (_addCommand = new CommandHandler(() => AddProduct(), _canExecute)); }
        }
        public ICommand OpenDialogCommand
        {
            get { return _openDialogCommand ?? (_openDialogCommand = new CommandHandler(() => OpenFileDialog(), _canExecute)); }
        }
        public ICommand UpdateCategoriesCommand
        {
            get { return _updateCategories ?? (_updateCategories
                    = new CommandHandler(() => UpdateCategories(), _canExecute)); }
        }
        public async void AddProduct()
        {
            Warning = "";
            _prestaService = new PrestaService(ConfigurationManager.AppSettings["currentSite"]);
            try
            {
                var product = new product();
                product.name.Add(new language(1, Name));
                product.description.Add(new language(1, Description));
                product.price = Convert.ToDecimal(Price);
                product.link_rewrite.Add(new language(1, Name));
                product.id_category_default = SelectedCategories.Last().Id;
                BooleanFlag = false;
                await Task.Run(() =>
                {
                    Status = "Добавляем товар...";
                    _prestaService.CreateProduct(product, Images);
                    Status = "Товар успешно добавлен";
                    Thread.Sleep(2000);
                    Name = "";
                    Description = "";
                    Price = "";
                    Filter = "";
                    ImagesCount = "";
                    Images.Clear();
                    Status = "Можно добавить новый товар.";
                });
                Categories.Clear();
                BooleanFlag = true;
            }
            catch (InvalidOperationException)
            {
                Warning = "Заполните все поля";
            }
            catch (FormatException e)
            {
                Warning = "Некорректно введены данные";
            }
            catch (Exception e)
            {
                Warning = e.Message;
            }

        }

        public async void UpdateCategories()
        {
            _prestaService = new PrestaService(ConfigurationManager.AppSettings["currentSite"]);
            var cats = new List<TemporaryCategory>();
            await Task.Run(() =>
            {
                cats = _prestaService.GetCategories();
            });
            Categories.Clear();
            foreach (var cat in cats)
            {
                if (_filter == null || cat.Name.ToLower().Contains(_filter.ToLower()))
                    Categories.Add(cat);
            }
            
        }
        public async void OpenFileDialog()
        {
            var openDialog = new OpenFileDialog();
            openDialog.Title = "Test";
            openDialog.AllowMultiple = true;
            var files = await openDialog.ShowAsync(new Window());
            if(files != null)
            Images.AddRange(files);
            ImagesCount = $"Добавлено изображений: {Images.Count}";
        }
    }
}
