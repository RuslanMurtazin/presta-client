﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using OmniXaml.ObjectAssembler;
using PrestaClientViaAvalonia.Models;

namespace PrestaClientViaAvalonia.ViewModels
{
    public class SettingsPageViewModel : BaseViewModel
    {
        private bool _canExecute;
        private ICommand _updateCommand;
        private string _text;
        private string _conf;
        private Site _selectedSite;
        public ObservableCollection<Site> Sites { get; set; }
        public SettingsPageViewModel()
        {
            Conf = ConfigurationManager.AppSettings["currentSite"];
            _canExecute = true;
            Sites = new ObservableCollection<Site>(new List<Site>
            {
                new Site{Url = "sport16.ru"},
                new Site{Url = "fit16.ru"},
                new Site{Url = "kettler16.ru"},
                new Site{Url = "sportb9q.bget.ru"}
            });
            SelectedSite = Sites.First();
            Conf = $"Текущий сайт {ConfigurationManager.AppSettings["currentSite"]}.";
        }

        public Site SelectedSite
        {
            get { return _selectedSite; }
            set
            {
                _selectedSite = value;
                OnPropertyChanged("SelectedSite");
            }
        }

        public string Conf
        {
            get { return _conf; }
            set
            {
                _conf = value;
                OnPropertyChanged("Conf");
            }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                OnPropertyChanged("Text");
            }
        }

        public ICommand UpdateCommand
        {
            get { return _updateCommand ?? (_updateCommand = new CommandHandler(() => ChangeConfig(), _canExecute)); }
        }

        public void ChangeConfig()
        {
            System.Configuration.Configuration currentConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            currentConfig.AppSettings.Settings["currentSite"].Value = SelectedSite.Url;
            currentConfig.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            Conf = $"Текущий сайт {ConfigurationManager.AppSettings["currentSite"]}.";
        }
    }
}
