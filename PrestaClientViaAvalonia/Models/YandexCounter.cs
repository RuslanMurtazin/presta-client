﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrestaClientViaAvalonia.Models
{
    public class YandexCounter
    {
        public string Id { get; set; }
        public string Site { get; set; }
        public int Users { get; set; }
        public int Visits { get; set; }
        public int PageViews { get; set; }
    }
}
