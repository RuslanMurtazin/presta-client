﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace PrestaClientViaAvalonia.Models
{
    public class Site
    {
        public string Url { get; set; }
        public string ApiPresta { get; set; }
        public string ApiYandex { get; set; }
        public string CounterId { get; set; }
    }
}
