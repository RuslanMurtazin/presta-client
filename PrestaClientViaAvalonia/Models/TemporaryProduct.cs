﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrestaClientViaAvalonia.Models
{
    public class TemporaryProduct
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string VendorCode { get; set; }
        public string Description { get; set; }
    }

    public class ReportProduct
    {
        public string Name { get; set; }
        public decimal OldPrice { get; set; }
        public decimal NewPrice { get; set; }
    }
}
