﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PrestaClientViaAvalonia.ViewModels;

namespace PrestaClientViaAvalonia.Pages
{
    public class ProductInsertPage : UserControl
    {
        public ProductInsertPage()
        {
            this.InitializeComponent();
            DataContext = new ProductInsertViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
