﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PrestaClientViaAvalonia.ViewModels;

namespace PrestaClientViaAvalonia.Pages
{
    public class CatalogSyncPage : UserControl
    {
        public CatalogSyncPage()
        {
            this.InitializeComponent();
            DataContext = new CatalogSyncViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
