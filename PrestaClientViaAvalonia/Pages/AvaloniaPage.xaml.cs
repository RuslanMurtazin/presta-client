﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PrestaClientViaAvalonia.ViewModels;

namespace PrestaClientViaAvalonia.Pages
{
    public class AvaloniaPage : UserControl
    {
        public AvaloniaPage()
        {
            this.InitializeComponent();
            DataContext = new AvaloniaViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
