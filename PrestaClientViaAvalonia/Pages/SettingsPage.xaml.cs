﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PrestaClientViaAvalonia.ViewModels;

namespace PrestaClientViaAvalonia.Pages
{
    public class SettingsPage : UserControl
    {
        public SettingsPage()
        {
            this.InitializeComponent();
            DataContext = new SettingsPageViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
