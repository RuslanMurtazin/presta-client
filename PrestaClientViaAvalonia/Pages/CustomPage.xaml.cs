﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PrestaClientViaAvalonia.ViewModels;

namespace PrestaClientViaAvalonia.Pages
{
    public class CustomPage : UserControl
    {
        public CustomPage()
        {
            this.InitializeComponent();
            DataContext = new CustomViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
