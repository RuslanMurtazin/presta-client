﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using PrestaClientViaAvalonia.ViewModels;

namespace PrestaClientViaAvalonia.Pages
{
    public class StatsPage : UserControl
    {
        public StatsPage()
        {
            this.InitializeComponent();
            DataContext = new StatsPageViewModel();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
