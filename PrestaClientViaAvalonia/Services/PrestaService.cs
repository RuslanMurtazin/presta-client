﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Bukimedia.PrestaSharp.Entities;
using Bukimedia.PrestaSharp.Factories;
using CsvHelper;
using PrestaClientViaAvalonia.Models;

namespace PrestaClientViaAvalonia.Services
{
    public class PrestaService
    {
        private string _baseUrl;
        private string _apiKey;
        private string _password = "";
        private ProductFactory _productFactory;
        private ImageFactory _imageFactory;
        private CategoryFactory _categoryFactory;
        public PrestaService(string url)
        {
            _baseUrl = $"http://{url}/api";
            _apiKey = ConfigurationManager.AppSettings[ConfigurationManager.AppSettings["currentSite"]];
            _productFactory = new ProductFactory(_baseUrl,_apiKey,_password);
            _imageFactory = new ImageFactory(_baseUrl, _apiKey, _password);
            _categoryFactory = new CategoryFactory(_baseUrl, _apiKey, _password);
        }

        public List<TemporaryCategory> GetCategories()
        {
            var categories = _categoryFactory.GetAll();
            var tmpCat = new List<TemporaryCategory>();
            foreach (var cat in categories)
            {
                var tc = new TemporaryCategory();
                tc.Id = cat.id;
                tc.Name = cat.name.First().Value;
                tmpCat.Add(tc);
            }
            return tmpCat;
        }

        public void GetProductsByFilter(List<TemporaryCategory> categories, string path)
        {
            
            var tmpProducts = new List<TemporaryProduct>();
            foreach (var category in categories)
            {
                var filter = new Dictionary<string, string>();
                filter.Add("id_category_default",category.Id.ToString());
                var products = _productFactory.GetByFilter(filter, null, null);
                foreach (var product in products)
                {
                    var tmpProduct = new TemporaryProduct
                    {
                        Id = product.id,
                        Name = product.name.First().Value,
                        Price = product.price,
                        VendorCode = product.reference,
                        Description = product.description.First().Value
                    };
                    tmpProducts.Add(tmpProduct);
                }
            }
           
            using (StreamWriter sw = new StreamWriter(
                new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite),
                Encoding.UTF8
            ))
            {
                var csv = new CsvWriter(sw);
                csv.WriteRecords(tmpProducts);
            }
            Process.Start(path);
        }
        public List<product> GetProducts(string path)
        {
            var products = _productFactory.GetAll();
            var tmpProducts = new List<TemporaryProduct>();
            foreach (var product in products)
            {
                var tmpProduct = new TemporaryProduct
                {
                    Id = product.id,
                    Name = product.name.First().Value,
                    Price = product.price,
                    VendorCode = product.reference,
                    Description = product.description.First().Value
                };
                tmpProducts.Add(tmpProduct);
            }
            using (StreamWriter sw = new StreamWriter(
                new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite),
                Encoding.UTF8
            ))
            {
                var csv = new CsvWriter(sw);
                csv.WriteRecords(tmpProducts);
            }
            Process.Start(path);
            return products;
        }

        public void UpdateProducts(string path)
        {
            using (var sr = File.OpenText(path))
            {
                var csv = new CsvReader(sr);
                var records = csv.GetRecords<TemporaryProduct>().ToList();
                var products = _productFactory.GetAll();
                foreach (var product in products)
                {
                    var tmpProduct = records.SingleOrDefault(x => x.Id == product.id);
                    product.name.First().Value = tmpProduct.Name;
                    product.price = tmpProduct.Price;
                    product.description.First().Value = tmpProduct.Description;
                    _productFactory.Update(product);
                }
            }
        }

        public void CreateProduct(product product_, List<string> images)
        {
            var categoriesId = GetParentsCategories((int)product_.id_category_default);
            var categories = new List<Bukimedia.PrestaSharp.Entities.AuxEntities.category>();
            foreach (var id_ in categoriesId)
            {
                var category = new Bukimedia.PrestaSharp.Entities.AuxEntities.category{id = id_};
                categories.Add(category);
            }
            product_.associations.categories = categories;
            product_.show_price = 1;
            product_.available_for_order = 1;
            product_.active = 1;
            _productFactory.Add(product_);
            if(images.Count >0)
            AddImages(images);
        }

        public int SyncCatalogsNeotren(bool needReport, string path)
        {
            string URLString = "http://neotren.ru/xml.php";
            string xmlStr = String.Empty;
            using (WebClient wc = new WebClient())
            {
                xmlStr = wc.DownloadString(URLString);
            }

            XDocument xdoc = XDocument.Parse(xmlStr);

            var items = xdoc.Element("catalog").Element("shop").Element("offers").Elements("offer")
                .Select(x =>
                new TemporaryProduct
                {
                    Name = x.Element("name").Value,
                    Price = Convert.ToDecimal(x.Element("price").Value)
                });
            int i = 0;
            var itemsFromFit = _productFactory.GetAll()
                .Where(x => items.Any(z => z.Name == x.name.First().Value) && x.active == 1);
            var report = new List<ReportProduct>();
            foreach (var itemFit in itemsFromFit)
            {
                var newPrice = items.Where(x => x.Name.ToLower() == itemFit.name.First().Value.ToLower())
                    .Select(x => x.Price).First();
                if (!newPrice.Equals(itemFit.price))
                {
                    report.Add(new ReportProduct{Name = itemFit.name.First().Value, NewPrice = newPrice, OldPrice = itemFit.price});
                    i++;
                    itemFit.price = newPrice;
                    _productFactory.Update(itemFit);
                }
            }

            if (!needReport) return i;
            using (StreamWriter sw = new StreamWriter(
                new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite),
                Encoding.UTF8
            ))
            {
                var csv = new CsvWriter(sw);
                csv.WriteRecords(report);
            }
            return i;
        }

        public int SyncCatalogsWeekBld(bool needReport, string path)
        {
            string URLString = "http://www.weekend-billiard.ru/exports/ym_export.php";
            string xmlStr = String.Empty;
            using (WebClient wc = new WebClient { Encoding = Encoding.UTF8 })
            {
                xmlStr = wc.DownloadString(URLString);
            }

            XDocument xdoc = new XDocument();
            xdoc = XDocument.Parse(xmlStr);
            var items = xdoc.Element("yml_catalog").Element("shop").Element("offers").Elements("offer")
                .Where(x => x.Element("vendorCode") != null)
                .Select(x =>
                    new TemporaryProduct
                    {
                        Name = x.Element("model").Value,
                        Price = Convert.ToDecimal(x.Element("price").Value),
                        VendorCode = Regex.Replace(x.Element("vendorCode").Value, @"\t|\n|\r", "")
                    });
            int i = 0;
            var report = new List<ReportProduct>();
            var itemsFromPresta = _productFactory.GetAll().Where(x => items.Any(z => z.VendorCode.Equals(x.reference)) && x.active == 1 && x.reference != null);
            foreach (var item in itemsFromPresta)
            {
                var newPrice = items.Where(x => x.VendorCode.Contains(item.reference)).Select(x => x.Price).First();
                if (!newPrice.Equals(item.price))
                {
                    report.Add(new ReportProduct { Name = item.name.First().Value, NewPrice = newPrice, OldPrice = item.price });
                    i++;
                    item.price = newPrice;
                    _productFactory.Update(item);
                    //to do 
                }
            }
            if (needReport)
            {
                using (StreamWriter sw = new StreamWriter(
                    new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite),
                    Encoding.UTF8
                ))
                {
                    var csv = new CsvWriter(sw);
                    csv.WriteRecords(report);
                }
            }
            return i;
        }

        public int GetProductsCount()
        {
            if (_baseUrl.Contains("fit16"))
                return 2918;
            return _productFactory.GetIds().Count;
        }
        private void AddImages(List<string> images)
        {
            var id = _productFactory.GetIds().Max();
            foreach (var image in images)
            {
                byte[] imageBytes = File.ReadAllBytes(image);
                if (imageBytes.Length < 1024 * 1024 * 3)
                    _imageFactory.AddProductImage(id, imageBytes);
            }
        }

        private List<int> GetParentsCategories(int categoryId)
        {
            var categories = _categoryFactory.GetAll();
            var categoriesId = new List<int>();
            while (categoryId != 1)
            {
                categoriesId.Add(categoryId);
                var tmp = categories.First(c => c.id == categoryId);
                categoryId = GetParentID(tmp);
            }
            return categoriesId;
        }

        private int GetParentID(category category)
        {
            return (int)category.id_parent;
        }


    }
}
