﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using PrestaClientViaAvalonia.Models;
using RestSharp;
using RestSharp.Extensions;

namespace PrestaClientViaAvalonia.Services
{
    public class AnalyticsService
    {
        private RestClient _restClient;

        public AnalyticsService()
        {
            _restClient = new RestClient("https://api-metrika.yandex.ru/stat/v1/");
        }

        public async Task<List<YandexCounter>> GetStats()
        {
            var request = new RestRequest("data",Method.GET);
            request.AddParameter("metrics", "ym:s:users,ym:s:visits,ym:s:pageviews");
            request.AddParameter("dimensions", "ym:s:startURLDomain");
            request.AddParameter("date1", "today");
            request.AddParameter("offset", "1");
            request.AddParameter("ids", "28022205,27727917,25403033");
            request.AddParameter("oauth_token", "AQAAAAANPzoqAAS2H6JR6t2DWEq_kWPAtsllXCQ");
            var content = "";
            await Task.Run(() =>
            {
                var response = _restClient.Execute(request);
                content = response.Content;
            });
            var data = JArray.Parse(JObject.Parse(content)["data"].ToString());
            var ids = JArray.Parse(JObject.Parse(content)["query"]["ids"].ToString());
            var counters = new List<YandexCounter>();
            int i = 0;
            foreach (var tmp in data)
            {
                var counter = new YandexCounter();
                counter.Id = ids[i].ToString();
                counter.Users = Convert.ToInt32(tmp["metrics"][0]);
                counter.Visits = Convert.ToInt32(tmp["metrics"][1]);
                counter.PageViews = Convert.ToInt32(tmp["metrics"][2]);
                counter.Site = tmp["dimensions"][0]["name"].ToString();
                Console.WriteLine(tmp["dimensions"][0]["name"]);
                counters.Add(counter);
                i++;
            }
            return counters;
        }

        public async Task DownloadStats(string path, string id, string period)
        {
            switch (id)
            {
                case "fit16.ru":
                    id = "27727917";
                    break;
                case "sport16.ru":
                    id = "28022205";
                    break;
                case "kettler16.ru":
                    id = "25403033";
                    break;
            }
            switch (period)
            {
                case "Неделя":
                    period = "7daysAgo";
                    break;
                case "Месяц":
                    period = "30daysAgo";
                    break;
                case "Квартал":
                    period = "90daysAgo";
                    break;
            }
            var request = new RestRequest("data.csv", Method.GET);
            request.AddParameter("dimensions", "ym:s:lastSearchEngine,ym:s:lastSearchPhrase,ym:s:startURL");
            request.AddParameter("metrics", "ym:s:visits,ym:s:pageviews,ym:s:bounceRate,ym:s:avgVisitDurationSeconds,ym:s:sumGoalReachesAny");
            request.AddParameter("date1", period);
            request.AddParameter("filters", "ym:s:lastTrafficSource=='organic'");
            request.AddParameter("ids", id);
            request.AddParameter("include_undefined", "true");
            request.AddParameter("oauth_token", "AQAAAAANPzoqAAS2H6JR6t2DWEq_kWPAtsllXCQ");
            await Task.Run(() =>
            {
                _restClient.DownloadData(request).SaveAs(path); ;
            });
        }
    }
}
